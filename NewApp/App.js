import React, { Component } from 'react';
import { AppRegistry, Image, View, Text } from 'react-native';

export default class Bananas extends Component {
  render() {
    let pic = {
      uri: 'https://static.maonaroda4x4.com.br/media/catalog/product/cache/1/thumbnail/600x/17f82f742ffe127f42dca9de82fb58b1/2/0/2013_08_20_16_28_50_1.jpg'
    };
    return (
      
      <View style={{flex: 1}}>
        <Image source={pic} style={{flex: 1, width: 200 , height: 200}}/>
         <Text>
        Imagem 1
        </Text>
        <Image source={pic} style={{flex: 2, width: 200 , height: 200}}/>
        <Text>
        Imagem 2
        </Text>

      </View>

    );
  }
}

// skip this line if using Create React Native App
AppRegistry.registerComponent('AwesomeProject', () => Bananas);
